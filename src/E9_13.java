///////////////////////////////////////////////////////////
// Lily Mendoza
// CS 49J
// 10/1/19
// This program will take two integers from the user and
// constructs a rectangle using java class Rectangle and
// subclass BetterRectangle. It then calculates and prints
// the area and perimeter.
///////////////////////////////////////////////////////////

import java.awt.Rectangle;              // importing Rectangle class
import java.util.Scanner;               // importing package to user Scanner

public class E9_13 {
    // creating BetterRectangle sub class of java Rectangle class
    public static class BetterRectangle extends Rectangle {
        // constructor that takes two integers h and w
        BetterRectangle(int h, int w) {
            setSize(w, h);                  // calling Rectangle method setSize to set height and width
            setLocation(120, 10);     // calling Rectangle method setLocation
        }
        // method to calculate and return area
        public int getArea() {
            return (height * width);
        }
        // method to calculate and return perimeter
        public int getPerimeter() {
            return ((2*height) + (2*width));
        }
    }
    public static void main(String[] args) {
        // greeting to user that program is starting
        System.out.println("Let's create a rectangle!");
        // prompting user to enter an integer for height
        System.out.println("Please enter an integer for height: ");
        // setting user input to variable height
        Scanner h = new Scanner(System.in);
        int height = h.nextInt();
        // prompting user to enter an integer for width
        System.out.println("Please enter an integer for width: ");
        // setting user input to variable width
        Scanner w = new Scanner(System.in);
        int width = w.nextInt();
        // constructing new rectangle called newRectangle
        BetterRectangle newRectangle = new BetterRectangle(height, width);
        // calling getArea and printing area
        System.out.println("The rectangles area is: " + newRectangle.getArea());
        // calling getPerimeter and printing perimeter
        System.out.println("The rectangles perimeter is: " + newRectangle.getPerimeter());
    }
}
